#install & setup mailhog
# ref : https://gist.github.com/opi/2ebea267a8a0435a85a9ac4594e5afcc
apt update
apt install wget

wget https://github.com/mailhog/MailHog/releases/download/v1.0.0/MailHog_linux_amd64 -O /opt/mailhog
chmod +x /opt/mailhog

#sed -i 's/;sendmail_path/sendmail_path = "\/opt\/mailhog sendmail test@example.org"/g' /etc/php/8.1/apache2/php.in

#Pour image drupal :
sed -i 's/;sendmail_path =/sendmail_path = "\/opt\/mailhog sendmail test@example.org"/g' /usr/local/etc/php/php.ini-production
sed -i 's/;sendmail_path =/sendmail_path = "\/opt\/mailhog sendmail test@example.org"/g' /usr/local/etc/php/php.ini-development

echo "-----Installing SMTP Modules------"
cd /opt/drupal
composer require drupal/smtp
composer require drupal/phpmailer_smtp
echo "-----Restarting Apache------"
service apache2 restart

cat <<EOF > /etc/systemd/system/mailhog.service
[Unit]
Description=MailHog Email Catcher
After=syslog.target network.target

[Service]
Type=simple
ExecStart=/opt/mailhog
StandardOutput=journal
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

cd /opt/
echo "-----Run Mailhog------"
./mailhog
