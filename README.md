# Drupal : reset password poisonning flaw

## Introduction

The objective of this project is to implement a proof of concept on the configuration flaw present on an initial drupal installation, which can lead to a total takeover of a server based on the Drupal CMS.

This PoC is based on Bogdan Tiron's article that I suggest you to read before going further: <br />
- https://fortbridge.co.uk/research/drupal-insecure-default-leads-to-password-reset-poisoning/

## The Issue 

This vulnerability has been present on the Drupal CMS since 2014: 

_"An attacker can construct an HTTP POST request that changes the domain used in the password reset link._

_For example, a password reset link for a Drupal site at example.com_
_might look like:_

http://example.com/?q=user/reset/1/123abc

_but the attacker can change the domain to anything:_

http://foo.com/?q=user/reset/1/123abc

_This can be exploited if the webserver is configured to forward any request to Drupal regardless of the domain name used in the request (catch all config)."_

## The environment

The proof of concept is based on the latest version of drupal at the time of testing, namely version 9.4.8.

## Set Up

In order to test the vulnerability, you must first clone the :

`$ git clone https://gitlab.com/creupsy/drupal_reset_password_poisonning_poc.git`

You must first have docker and docker-compose installed on your test environment.

Then you can run the following command:

`$ docker-compose up`

The drupal application is then accessible from your web browser at the following URL:

- http://127.0.0.1:8080

And the mail server at the following address :

- http://127.0.0.1:8025

You can then launch the installation of drupal, here is for the test the various information to validate the installation:

![alt text](screen/Sélection_011.png "DB connect")
 

Here it is accessory, but here is what I entered :

![alt text](screen/Sélection_012.png "web config")


Once the installation is finished you must go to the extensions and install the two following extensions:

![alt text](screen/Sélection_016.png "module install")

Go back on "SMTP Authentication Support -> "Configure"

Then configure the SMTP module as follows to connect to our local MailHog server.

![alt text](screen/Sélection_017.png "SMTP config")

You can send a test mail in order to validate its good functioning:

![alt text](screen/Sélection_018.png "SMTP test mail")

![alt text](screen/Sélection_019.png "SMTP email")


And here we are ready.

## PoC

We can then try to exploit the vulnerability.

To do this we need to log out, then go to the forgotten password tab.
In a typical case, if we enter our email address and validate, we will see that we receive an email with a link to reset our unique password.

![alt text](screen/Sélection_001.png "Reset password form")

![alt text](screen/Sélection_007.png "Reset password link")

However, if we try again, but we intercept the request and modify the "host" header with a domain under our control (here we used Burp's "Collaborator" feature), we see in the response a redirection to our domain. So far, nothing especially abnormal.

![alt text](screen/Sélection_002.png "edited request")

![alt text](screen/Sélection_003.png "Redirection")

![alt text](screen/Sélection_005.png "Redirection2")

However, when we check our mailbox, we notice that the password reset link no longer leads to the legitimate application, but to the attacker's domain.

![alt text](screen/Sélection_006.png "Reset password poisoning")

All the user has to do is click on the link, and the attacker receives the unique password reset link.
Once the attacker has this link, he can reset the application administrator's password.

![alt text](screen/Sélection_008.png "Collabborator request")
![alt text](screen/Sélection_009.png "Reset password form")
![alt text](screen/Sélection_010.png "Reseting password")

Done ! We are administrator of the Drupal app !

## Mitigations

Drupal has not fixed this flaw present since 2014, so this vulnerability is present by default. However, it is possible to protect against it with the option added since Drupal 8 "trusted host patterns":

"Drupal 8+ can be configured to use the Symfony trusted host mechanism to prevent HTTP Host header spoofing. To enable the trusted host mechanism, you enable your allowable hosts in $settings['trusted_host_patterns'] in settings.php. This should be an array of regular expression patterns, without delimiters, representing the hosts you would like to allow. If the Host header of the HTTP request does not match the defined patterns, Drupal will respond with HTTP 400 with a message The provided host name is not valid for this server.

In the example below, the site is only allowed to run from www.example.com.

```
$settings['trusted_host_patterns'] = [
  '^www\.example\.com$',
];"
```

Moreover with different tests on different environment it seems that some configuration even without the use of the option "trusted hosts" are not vulnerable to this vulnerability. This may depend on the configuration of the web server (apache, nginx...). We have observed that when using a reverse proxy the attack does not seem to work. This part is still to be validated in order to highlight exactly in which case the attack can work or not

